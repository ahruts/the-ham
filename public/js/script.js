// tabs
const tabTitles = [...document.querySelectorAll(".services-tab-title")];
const tabText = [...document.querySelectorAll(".services-tab-text")];

tabTitles.forEach((item) => {
  item.addEventListener("click", (event) => {
    removeActiveClass(tabTitles);
    removeActiveClass(tabText);
    event.target.classList.add("active");
    const dataTab = event.target.dataset.tab;
    const dataTabText = tabText.find((item) => {
      return item.dataset.tab === dataTab;
    });
    dataTabText.classList.add("active");
  });
});

function removeActiveClass(items) {
  items.forEach((item) => {
    item.classList.remove("active");
  });
}

// // work-filter
const workFilterTabs = [...document.querySelectorAll(".work-filter-title")];
const workFilterItems = [...document.querySelectorAll(".work-images-item")];
const loadmoreBtn = document.querySelector(".load-more-btn");
let workActiveItems = 12;

workFilterTabs.forEach((element) => {
  element.addEventListener("click", (event) => {
    removeActiveClass(workFilterTabs);
    event.target.classList.add("active");
    const dataTab = event.target.dataset.filter;
    workFilterItems.slice(0, workActiveItems).forEach((element) => {
      element.classList.remove("active");
      if (element.dataset.filter == dataTab || !dataTab) {
        element.classList.add("active");
      }
    });
  toggleLoadmoreBtn();
  });
});

// work-load-more

loadmoreBtn.addEventListener("click", (event) => {
  for (let i = workActiveItems; i < workActiveItems + 12; i++) {
    if (
      workFilterItems[i].dataset.filter ===
        document.querySelector(".work-filter-title.active").dataset.filter ||
      !document.querySelector(".work-filter-title.active").dataset.filter
    ) {
      workFilterItems[i].classList.add("active");
    }
  }
  workActiveItems += 12;
  toggleLoadmoreBtn();
});

function toggleLoadmoreBtn() {
  const activeDataset = document.querySelector(".work-filter-title.active")
    .dataset.filter;
  const activeDatasetItems = [
    ...document.querySelectorAll(".work-images-item.active"),
  ];

  if (
    activeDatasetItems.length ===
      [...document.querySelectorAll(`[data-filter = "${activeDataset}"]`)]
        .length -
        1 ||
    activeDatasetItems.length === workFilterItems.length
  ) {
    loadmoreBtn.style.display = "none";
  } else if (
    activeDatasetItems.length !==
    [...document.querySelectorAll(`[data-filter = "${activeDataset}"]`)]
      .length -
      1
  ) {
    loadmoreBtn.style.display = "block";
  }
}

// search-btn

$(document).ready(function () {
  $(".nav-search-img").click(function () {
    $(".nav-input").toggleClass("active");
  });

  // header-links
  
  [...document.querySelectorAll(".nav-link")].forEach((item) => {
    item.addEventListener('click', (event) => {
       event.preventDefault();
      const target = event.target.getAttribute("href");
      document.querySelector(target).scrollIntoView({
        behavior: "smooth",
        block: "start",
      });
})
})

  // slider

  $(".reviews-slider-big").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    asNavFor: ".reviews-slider",
    draggable: false,
    fade: true,
  });
  $(".reviews-slider").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    focusOnSelect: true,
    asNavFor: ".reviews-slider-big",
    centerPadding: "10px",
    centerMode: true,
  });
});
